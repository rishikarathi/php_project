Assignment Question are as follows:

1. Program to find whether a year is a LEAP year or not

2. Write a program to find second highest number in an array.

3. $color = array('white', 'green', 'red'')
Write a PHP script which will display the colors in the following way :
Output :
green
red
white

4. Create a PHP script which displays the capital and country name from the below array $ceu. Sort the list by the capital of the country.

$ceu = array( "Italy"=>"Rome", "Luxembourg"=>"Luxembourg", "Belgium"=> "Brussels", "Denmark"=>"Copenhagen", "Finland"=>"Helsinki", "France" => "Paris", "Slovakia"=>"Bratislava", "Slovenia"=>"Ljubljana", "Germany" => "Berlin", "Greece" => "Athens", "Ireland"=>"Dublin", "Netherlands"=>"Amsterdam", "Portugal"=>"Lisbon", "Spain"=>"Madrid", "Sweden"=>"Stockholm", "United Kingdom"=>"London", "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius", "Czech Republic"=>"Prague", "Estonia"=>"Tallin", "Hungary"=>"Budapest", "Latvia"=>"Riga", "Malta"=>"Valetta", "Austria" => "Vienna", "Poland"=>"Warsaw") ;

Sample Output :
The capital of Netherlands is Amsterdam
The capital of Greece is Athens
The capital of Germany is Berlin

5.  Write a PHP script to calculate and display average temperature, five lowest and highest temperatures.
Recorded temperatures : 78, 60, 62, 68, 71, 68, 73, 85, 66, 64, 76, 63, 75, 76, 73, 68, 62, 73, 72, 65, 74, 62, 62, 65, 64, 68, 73, 75, 79, 73

Expected Output :
Average Temperature is : 70.6
List of seven lowest temperatures : 60, 62, 63, 63, 64,
List of seven highest temperatures : 76, 78, 79, 81, 85,

6. Write a Program to create given pattern with * using for loop
Description:
Write a Program to create following pattern using for loops:

*
**
***
****
*****
******
*******
********

7. Write a program to print the below format :
1 5 9
2 6 10
3 7 11
4 8 12

8. write a program to calculate Electricity bill in PHP
Description: You need to write a PHP program to calculate electricity bill using if-else conditions.

Conditions:

For first 50 units – Rs. 3.50/unit
For next 100 units – Rs. 4.00/unit
For next 100 units – Rs. 5.20/unit
For units above 250 – Rs. 6.50/unit
You can use conditional statements.

9. Write a simple calculator program in PHP using switch case
Description:

You need to write a simple calculator program in PHP using switch case.

Operations:

Addition
Subtraction
Multiplication
Division

10. Write a php script to print any statement from a class function by only creating its object.

11. Write a program to reverse a string.

12. Write a program to check if a number is palindrome or not.

